FROM php:7.4
RUN apt-get update
RUN apt-get install -qq git curl libmcrypt-dev libbz2-dev libzip-dev
RUN apt-get clean
RUN docker-php-ext-install zip
RUN curl --silent --show-error "https://getcomposer.org/installer" | php -- --install-dir=/usr/local/bin --filename=composer
RUN composer global require "laravel/envoy"
