@servers(['web' => ['deploy@192.168.0.102']])

@story('deploy')
    git
    composer
    npm
@endstory

@task('git')
    echo '<<<<----- Cloning Repository --->>>>'
    cd /var/www/html/local.com
    php artisan up
    php artisan down
    git pull
@endtask

@task('composer')
    echo '<<<<---- Composer starts --->>>>'
    cd /var/www/html/local.com
    composer install --prefer-dist --no-scripts -q -o
    php artisan migrate --force
    php artisan config:clear
    php artisan cache:clear
@endtask

@task('npm')
    echo '<<<<---- Npm start --->>>>'
    cd /var/www/html/local.com
    source ~/.nvm/nvm.sh
    npm install
    npm run prod
    echo '<<<<---- hapus modules start --->>>>'
    rm -rf node_modules/
    echo '<<<<---- Hapus modules berhasil --->>>>'
    php artisan up
@endtask
