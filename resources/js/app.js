import Vue from "vue";
import App from "./components/App";
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap';

new Vue({
    el: '#app',
    render: h => h(App)
});
